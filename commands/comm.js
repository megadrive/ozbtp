'use strict'

const Command = require('../modules/commands').Command

const db = require('../db')
const u = require('../util')
const consts = require('../consts')

class AddComm extends Command {
  constructor () {
    super({
      'name': 'comadd',
      'permission': consts.MODERATOR | consts.BROADCASTER
    })
  }

  run (handler, args, event) {
    const commName = args[1]
    const commText = args.splice(2).join(' ')

    if (args.length < 2) return

    console.log(`creating new command ${commName} -- ${commText}`)

    db.models.Command.findOrCreate({
      where: {
        command: commName,
        channel: event.channel
      },
      defaults: {
        command: commName,
        text: commText,
        channel: event.channel
      }})
      .spread((command, created) => {
        if (created) {
          handler.say(event.channel, `Created new command ${command.command} -- ${command.text}`)
        } else {
          handler.say(event.channel, `Command ${command.command} already exists: ${command.text}`)
        }
      })
      .catch(console.error)
  }
}

class EditComm extends Command {
  constructor () {
    super({
      'name': 'comedit',
      'permission': consts.MODERATOR | consts.BROADCASTER
    })
  }

  run (handler, args, event) {
    const commName = args[1]
    const commText = args.splice(2).join(' ')

    if (args.length < 2) return

    console.log(`editing command ${commName} -- ${commText}`)
    db.models.Command.update({ text: commText }, {
      where: {
        command: commName,
        channel: event.channel
      }
    })
      .then(affected => {
        if (affected > 0) {
          handler.say(event.channel, `${commName} updated: ${commText}`)
        }
      })
  }
}

class DeleteComm extends Command {
  constructor () {
    super({
      'name': 'comdel',
      'permission': consts.MODERATOR | consts.BROADCASTER
    })
  }

  run (handler, args, event) {
    const commName = args[1]
    const commText = args.splice(2).join(' ')

    if (args.length < 1) return

    console.log(`deleting command ${commName} -- ${commText}`)
    db.models.Command.destroy({
      where: {
        command: commName,
        channel: event.channel
      }
    })
      .then(affected => {
        if (affected > 0) {
          handler.say(event.channel, `Deleted ${commName}.`)
        }
      })
  }
}

class HelpComm extends Command {
  constructor () {
    super({
      'name': 'commands'
    })
  }

  run (handler, args, event) {
    handler.say(event.channel, 'https://bitbucket.org/megadrive/ozbtp/src/master/docs/docs.md')
  }
}

class PermissionAccess extends Command {
  constructor () {
    super({
      'name': 'comperm',
      'permission': consts.MODERATOR | consts.BROADCASTER
    })
  }

  // !commperm !commandname permission
  run (handler, args, event) {
    const commandName = args[1]
    const permission = args[2].trim().toUpperCase()

    // @TODO: Make extensible, not hard coded
    const expectedPermissions = ['ANYONE', 'SUBSCRIBER', 'MODERATOR', 'BROADCASTER']
    if (expectedPermissions.includes(permission)) {
      db.models.Command.update({
        permission: permission
      },
      {
        where: {
          channel: event.channel,
          command: commandName
        }
      })
        .then(function () {
          handler.say(event.channel, `Permission for command ${commandName} was updated to ${permission}.`)
        })
    }
  }
}

module.exports = {
  AddComm: new AddComm(),
  EditComm: new EditComm(),
  DeleteComm: new DeleteComm(),
  HelpComm: new HelpComm(),
  PermissionAccess: new PermissionAccess()
}
