'use strict'

const Command = require('../modules/commands').Command
const hltb = require('howlongtobeat')
const hltbService = new hltb.HowLongToBeatService()
const u = require('../util')

const ignoredGames = [
  'IRL', 'Casino', 'Music', 'Creative', 'Always On'
]

class HowLongToBeat extends Command {
  constructor () {
    super({
      'name': 'howlong'
    })
  }

  run (handler, args, event) {
    const searchTerm = args.splice(1).join(' ')
    const self = this

    function findGame (searchTerm) {
      console.log(`[${self.name}] finding result for ${searchTerm}`)
      hltbService.search(searchTerm)
        .then(results => {
          const r = results[0]
          if (r) {
            let words = `${r.name} will take roughly ${r.gameplayMain} hours.`
            u.getShortenedUrl('https://howlongtobeat.com/game.php?id=' + r.id)
              .then((url) => {
                words += ` via ${url}`
                handler.say(event.channel, words)
              })
              .catch(e => {
                console.error(e)
                handler.say(event.channel, words)
              })
          } else {
            handler.say(event.channel, `Couldn't find ${searchTerm} on HowLongToBeat.`)
          }
        })
        .catch(console.error)
    }

    if (searchTerm) {
      findGame(searchTerm)
    } else {
      console.info(`[${self.name}] no search term supplied, getting game and using that instead.`)

      u.twitchApi(`users?login=${event.channel.substring(1)}`)
        .then(user => {
          const userid = user.data[0].id

          u.twitchApi(`channels/${userid}`, 'kraken')
            .then(channel => {
              if (ignoredGames.includes(channel.game) === false) {
                findGame(channel.game)
              } else {
                console.info(`[${self.name}] game is ${channel.game}; ignoring.`)
              }
            })
            .catch(console.error)
        })
        .catch(console.error)
    }
  }
}

module.exports = new HowLongToBeat()
