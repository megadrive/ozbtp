'use strict'

const { Command } = require('../modules/commands')
const Chance = require('chance'); const chance = new Chance()

class Lucky extends Command {
  constructor () {
    super({
      'name': 'lucky'
    })
  }

  run (handler, args, event) {
    let results = {
      heads: 0, tails: 0
    }
    for (var i = 0; i < 5000; i++) {
      results[chance.coin()]++
    }

    const say = {
      alive: ' survived the shot, what a lucky person!',
      dead: ' died. Whoops.'
    }

    let status = null
    if (results.heads > results.tails) {
      status = 'alive'
    } else {
      status = 'dead'
      handler.tmi.timeout(event.channel, event.userstate.username, 60, 'Russian Rouletted.')
        .then(function () {
          console.log(`[${event.channel}] ${event.userstate.username} timed out.`)
        })
        .catch(console.error)
    }

    handler.say(event.channel, event.userstate.username + say[status] + ` (h: ${results.heads}; t: ${results.tails})`)
  }
}

module.exports = new Lucky()
