'use strict'

const consts = require('../consts')
const Command = require('../modules/commands').Command

const request = require('superagent')
const conf = require('../config')

function getNewestYoutubeUrl () {
  return new Promise(function (resolve, reject) {
    request
      .get(conf.youtubeUrl)
      .end((err, res) => {
        if (err) throw Error(err)

        // let latestVid = /href="\/watch\?v=([A-Za-z0-9]+)"/i.exec(res.text)
        let match = /<a.+title="(.+?)".+href="\/(.+?)"/i.exec(res.text)
        let videoId = ''
        let videoTitle = ''
        if (match) {
          videoId = match[2]
          videoTitle = match[1]
        } else {
          reject(Error('RegExp found no video info.'))
        }

        resolve({
          url: `https://youtube.com/${videoId}`,
          title: videoTitle
        })
      })
  })
}

class Youtube extends Command {
  constructor () {
    super({
      'name': 'youtube',
      'permission': consts.ANYONE
    })
  }

  run (handler, args, event) {
    getNewestYoutubeUrl()
      .then(info => {
        handler.say(event.channel, `Newest YouTube video by ${event.channel.slice(1)} is ${info.title}: ${info.url}`)
      })
      .catch(console.error)
  }
}

module.exports = new Youtube()
