'use strict'

module.exports.ANYONE = 0
module.exports.SUBSCRIBER = 1
module.exports.MODERATOR = 2
module.exports.BROADCASTER = 4
