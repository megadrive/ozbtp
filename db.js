'use strict'

const conf = require('./config')
require('dotenv').config()

const Sequelize = require('sequelize')
const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: conf.ozbt.databaseFile,
  logging: function () {}
})

const EventEmitter = require('events')
const SQLize = new EventEmitter()

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')

    setup()
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  })

const Channel = sequelize.define('channel', {
  id: {
    type: Sequelize.INTEGER
  },
  channel: {
    primaryKey: true,
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  },
  userid: {
    type: Sequelize.INTEGER,
    allowNull: true
  }
})

const Command = sequelize.define('command', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  command: {
    type: Sequelize.STRING,
    allowNull: false
  },
  text: {
    type: Sequelize.STRING,
    allowNull: false
  },
  channel: {
    type: Sequelize.STRING,
    allowNull: false
  },
  permission: {
    type: Sequelize.STRING
  }
})

// Define relationships
Channel.hasMany(Command, { as: 'commands', foreignKey: 'channel' })

module.exports.models = {
  Channel: Channel,
  Command: Command
}

module.exports.db = sequelize
module.exports.events = SQLize

function setup () {
  sequelize.sync({
    alter: true
  })

  SQLize.emit('connected')
}
