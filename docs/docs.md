
# Commands

## !comadd commandName Text to display when triggered

*(mods)* Adds a command to the channel. When a user types a message starting with `commandName`, the bot will respond with the supplied text.

## !comedit commandName Text to display when triggered

*(mods)* Edits the command supplied from commandName with the supplied text. If you want to change the commandName, just create a new command.

## !comdel commandName

*(mods)* Deletes a command.

# Built-in Commands

## !commands

Responds with a link to this documentation.

## !howlong [game name]

Searches the howlongtobeat.com database for `game term` and responds with the main category length. If you omit the `game name`, it will get the current game being played by the streamer.

## !uptime

Gets the uptime of the current streamer.
