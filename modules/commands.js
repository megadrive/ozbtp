'use strict'

require('dotenv').config()
const admins = require('../config').twitch.admins
const db = require('../db')
const consts = require('../consts')

class Command {
  constructor (opts) {
    this.name = ''

    if (opts && typeof opts === 'object') {
      for (let k in opts) {
        this[k] = opts[k]
      }
    }
  }

  run (tmi, args, event) {}
}

class CommandsHandler {
  constructor (opts) {
    this.commands = new Map()
    this.cooldowns = {}

    if (opts && typeof opts === 'object') {
      for (let k in opts) {
        this[k] = opts[k]
      }

      if (!opts.tmi) throw new Error('Must supply a tmi.js instance.')
    }
  }

  add (commandObject) {
    this.commands.set(commandObject.name, commandObject)
    console.log(`added command ${commandObject.name}`)
  }

  addMany (arrayOfCommandObjects) {
    for (let i = 0; i < arrayOfCommandObjects.length; i++) {
      this.add(arrayOfCommandObjects[i])
    }
  }

  detect (channel, userstate, message) {
    if (process.env.ENV === 'debug' && !admins.includes(userstate.username)) return

    const index = message.indexOf(' ') > 0 ? message.indexOf(' ') : message.length
    let commandName = message.substring(1, index)
    let args = message.substring(1).split(' ')

    const eventData = {
      channel: channel,
      userstate: userstate,
      message: message
    }

    if (message.startsWith('!')) {
      console.log(`detected command ${commandName} with args ${args}. attempting to run..`)
      this.run(commandName, args, eventData)
    } else {
      this.detectChannelCommand(message.substring(0, index), args, eventData)
    }
  }

  run (commandName, args, event) {
    if (this.commands.has(commandName)) {
      const command = this.commands.get(commandName)
      if (command) {
        console.log(`running command ${commandName}`)
        this.hasPermission(commandName, args, event, command)
          .then((access) => {
            if (access.access && this.cooldownOk(commandName, event)) {
              command.run(this, args, event)
            }
          })
      }
    } else {
      console.error(`command ${commandName} was not found.. trying channel command`)
      this.detectChannelCommand(commandName, args, event)
    }
  }

  detectChannelCommand (commandName, args, event) {
    const command = event.message.split(' ')[0]
    db.models.Command.find({ where: { channel: event.channel, command: command } })
      .then((command) => {
        if (command && this.cooldownOk(commandName, event)) {
          this.say(event.channel, command.text)
        }
      })
  }

  hasPermission (commandName, args, event, ozbtCommand) {
    const userPermission = CommandsHandler.getPermissionFromEvent(event)

    return new Promise((resolve, reject) => {
      if (ozbtCommand) {
        let r = { access: true } // anyone
        if (ozbtCommand.permission) {
          r.access = ozbtCommand.permission & userPermission
        }
        resolve(r)
      } else {
        const command = event.message.split(' ')[0]
        db.models.Command.find({ where: { channel: event.channel, command: command } })
          .then((command) => {
            if (command) {
              if (command.permission) {
                const bit = consts[command.permission.toUpperCase()]
                if (bit) {
                  resolve({
                    access: command.permission & userPermission
                  })
                } else {
                  reject(Error(`Invalid permission ([${event.channel}] ${commandName}): ${command.permission}`))
                }
              } else {
                // Undefined/NULL (anyone)
                resolve({access: true})
              }
            }
          })
      }
    })
  }

  cooldownOk (commandName, event) {
    const self = this
    /**
     * Inline function since the update code is used more than once.
     */
    function updateCooldown (commandName, event) {
      console.info(`updated cooldown for ${commandName} in channel ${event.channel}`)
      self.cooldowns[event.channel][commandName] = Date.now()
    }

    let channelCooldowns = this.cooldowns[event.channel]
    if (!channelCooldowns) {
      this.cooldowns[event.channel] = {}
      channelCooldowns = this.cooldowns[event.channel]
    }

    if (channelCooldowns) {
      const cooldownTimestamp = channelCooldowns[commandName]
      if (cooldownTimestamp) {
        const diff = Date.now() - cooldownTimestamp
        if (diff > 5000) {
          updateCooldown(commandName, event)
          return true
        } else {
          return false
        }
      }

      updateCooldown(commandName, event)
      return true
    } else {
      console.error(`'channelCooldowns' doesn't exist in cooldownOk(). cooldowns will not work if this error exists.`)
    }

    return false
  }

  static getPermissionFromEvent (event) {
    let permission = consts.ANYONE

    if (event.userstate.subscriber) permission = consts.SUBSCRIBER
    if (event.userstate.mod) permission = consts.MODERATOR
    if (event.userstate.badges && event.userstate.badges.broadcaster) permission = consts.BROADCASTER

    return permission
  }

  say (channel, words) {
    this.tmi.say(channel, words)
  }
}

module.exports = {
  Command: Command,
  CommandsHandler: CommandsHandler
}
