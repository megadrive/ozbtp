'use strict'

const config = require('./config.json')
const tmi = require('tmi.js')
const u = require('./util')

const _CommandsHandler = require('./modules/commands').CommandsHandler
const db = require('./db')

const options = {
  options: {
    // debug: true
  },
  connection: {
    reconnect: true
  },
  identity: {
    username: config.twitch.user,
    password: config.twitch.oauth
  },
  channels: config.twitch.channels
}

const client = new tmi.client(options) // eslint-disable-line
const commandsHandler = new _CommandsHandler({tmi: client})

// Connect the client to the server..
client.connect()

// add commands
const HowLongToBeat = require('./commands/hltb')
const Uptime = require('./commands/uptime')
const {
  AddComm,
  EditComm,
  DeleteComm,
  HelpComm,
  PermissionAccess
} = require('./commands/comm')
const Youtube = require('./commands/youtube')
const Lucky = require('./commands/lucky')
commandsHandler.add(HowLongToBeat)
commandsHandler.add(Uptime)
commandsHandler.add(Youtube)
commandsHandler.addMany([AddComm, EditComm, DeleteComm, HelpComm, PermissionAccess])
commandsHandler.add(Lucky)

client.on('chat', (channel, userstate, message, self) => {
  if (self || !config.twitch.channels.includes(channel)) return

  commandsHandler.detect(channel, userstate, message)
})

client.on('join', (channel, username, self) => {
  if (self) {
    u.twitchApi(`users?login=${channel.substring(1)}`)
      .then(res => {
        db.models.Channel.create({
          channel: channel,
          userid: res.data[0].id
        })
          .then(() => {
            console.info(`created record for channel ${channel} (id: ${res.data[0].id})`)
          })
          .catch((err) => {
            if (!err.name.includes('Unique')) {
              console.error(err)
            }
          })
      })
  }
})
