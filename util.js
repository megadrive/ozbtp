'use strict'

const got = require('got')
const conf = require('./config')

module.exports = {
  /**
   * @returns object object with applicable keys overridden
   */
  applyOpts: function (overrides, defaults) {
    let def = defaults

    for (let k in overrides) {
      if (overrides.hasOwnProperty(k)) {
        def[k] = overrides[k]
      }
    }

    return def
  },

  getShortenedUrl: function (url) {
    return new Promise((resolve, reject) => {
      const opts = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          'Host': 'elbo.in',
          'Referer': 'https://elbo.in/'
        },
        // json: true,
        body: 'url=' + url
      }
      got.post(`https://elbo.in/~shorten`, opts)
        .then(res => {
          const j = JSON.parse(res.body)
          resolve('https://elbo.in/' + j.shorturl)
        })
        .catch(err => {
          throw new Error('Could not shorten link: ' + err)
        })
    })
  },

  twitchApi: function (endpoint, apiVersion) {
    apiVersion = apiVersion || 'helix'
    return new Promise((resolve, reject) => {
      if (endpoint && endpoint.length > 3) {
        var opts = {
          'json': true,
          'headers': {
            'Client-ID': conf.twitch.clientid,
            'Accept': 'application/vnd.twitchtv.v5+json'
          }
        }

        console.info(`[twitchApi] api request: ${endpoint}`)
        got(`https://api.twitch.tv/${apiVersion}/` + endpoint, opts)
          .then(res => {
            resolve(res.body)
          })
      }
    })
  },

  hasPermission: function (required, supplied) {
    return required & supplied
  }
}
